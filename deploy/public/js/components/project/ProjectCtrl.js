function ProjectCtrl(ProjectsService, TasksService, FilterService, $scope, $uibModal) {

    $scope.filters = FilterService.filters;
    $scope.projects = ProjectsService.get();
    $scope.selectedRow = null;
    $scope.newProject = {};

    $scope.projectActions = [
        {name: 'delete'},
        {name: 'edit'},
        {name: 'show statistic'}
    ];

    $scope.setProjectAction = function (action, project) {
        if (action.name === $scope.projectActions[0].name) {
            ProjectsService.remove(project);
        } else if (action.name === $scope.projectActions[1].name) {
            $scope.editProject(project);
        } else if (action.name === $scope.projectActions[2].name) {
            $scope.showStatistic(project);
        }
    };

    $scope.editProject = function (project) {
        $uibModal.open({
            animation: true,
            templateUrl: 'js/components/project/parts/project-modal.html',
            controller: 'ProjectModalInstanceCtrl',
            size: 'sm',
            resolve: {
                project: function () {
                    return project;
                }
            }
        });
    };

    $scope.showStatistic = function (project) {
        $uibModal.open({
            animation: true,
            templateUrl: 'js/components/project/parts/project-statistic.html',
            controller: 'ProjectModalInstanceCtrl',
            resolve: {
                project: function () {
                    return project;
                }
            }
        });
    };

    $scope.setClickedRow = function (index) {  //function that sets the value of selectedRow to current index
        $scope.selectedRow = index;
    };

    $scope.filterProjects = function (filter) {
        TasksService.setFilter(filter);
    };

    $scope.addProject = function (project) {
        ProjectsService.addNewProject(project);
        cleanFields();
    };

    $scope.refreshTasks = function (project) {
        TasksService.refresh(project);
    };

    $scope.removeProject = function (project) {
        ProjectsService.remove(project);
    };

    function cleanFields() {
        $scope.newProject.title = "";
    }
}

module.exports = ProjectCtrl;
