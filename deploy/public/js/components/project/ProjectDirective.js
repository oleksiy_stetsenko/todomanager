function ProjectDirective() {
    return {
        controller: 'ProjectsCtrl',
        controllerAs: 'projectsCtrl',
        templateUrl: 'js/components/project/project.html'
    }
}

module.exports = ProjectDirective;