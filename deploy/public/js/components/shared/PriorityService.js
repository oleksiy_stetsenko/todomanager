function PriorityService() {


    this.priorityEnum = {
        LOW: 1,
        MEDIUM: 2,
        HIGH: 3,
        properties: {
            1: {name: "Low", value: 1, color: 'green'},
            2: {name: "Medium", value: 2, color: 'yellow'},
            3: {name: "High", value: 3, color: 'red'}
        }
    };
}

module.exports = PriorityService;