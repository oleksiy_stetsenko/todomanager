function TasksService(ProjectsService) {

    var service = this;
    service.tasks = [];

    service.get = function () {
    };

    service.setFilter = function (filter) {
        var result = [];
        ProjectsService.projects.forEach(function (project) {
            filterSubTasks(project.tasks)
        });

        service.title = filter.title;
        service.tasks = result;
        return result;

        function filterSubTasks(tasks) {
            tasks.forEach(function (task) {
                if (task.tasks) {
                    filterSubTasks(task.tasks)
                }
                if (filter.value === 'all') {
                    addTaskToResult(task);
                } else if (filter.value === 'low' || filter.value === 'middle' || filter.value === 'high') {
                    if (task.priority && task.priority == filter.id) {
                        addTaskToResult(task);
                    }
                } else if (filter.value === 'day') {
                    if (task.dueDateTime && isToday(task.dueDateTime)) {
                        addTaskToResult(task);
                    }
                } else if (filter.value === 'week') {
                    if (task.dueDateTime && isThisWeek(task.dueDateTime)) {
                        addTaskToResult(task);
                    }
                }
            });
        }

        function isToday(dueDateTime) {
            var assertedDate = new Date(dueDateTime).setHours(0, 0, 0, 0);

            var date = new Date();
            var todayDate = date.setHours(0, 0, 0, 0);

            return assertedDate == todayDate;
        }

        function isThisWeek(dueDateTime) {
            var curr = new Date(); // get current date
            var first = curr.getDate() - curr.getDay();
            var last = first + 6;

            var firstDayOfWeek = new Date(curr.setDate(first)).setHours(0, 0, 0, 0);
            var lastDayOfWeek = new Date(curr.setDate(last)).setHours(0, 0, 0, 0);

            var date = new Date(dueDateTime);
            var comparedDate = date.setHours(0, 0, 0, 0);

            if (firstDayOfWeek < comparedDate && lastDayOfWeek > comparedDate) {
                return true;
            }
        }

        function addTaskToResult(task) {
            var clonedTask = angular.copy(task);
            delete clonedTask.tasks;
            result.push(clonedTask);
        }
    };

    service.addTask = function (task) {
        service.tasks.push(task);
    };

    service.getTasksByProject = function (project) {
        service.tasks = ProjectsService.getTasksByProject(project)
    };

    service.refresh = function (project) {
        service.tasks = project.tasks;
        service.title = project.title;
        parseDate();
    };

    service.remove = function (task) {
        service.tasks = service.tasks.filter(function (item) {
            return item !== task;
        });
    };

    service.updateTask = function (currentTask) {
        service.tasks.forEach(function (task, index) {
            if (task.id === currentTask.id) {
                service.tasks[index] = currentTask;
            }
        })
    };

    service.add = function (task) {
        service.tasks.push(task);
    };

// Private methods

    function parseDate() {
        service.tasks.forEach(function (task) {
            task.dueDateTime = new Date(task.dueDateTime);
        });
    }
}

module.exports = TasksService;