function ModalInstanceCtrl($scope, $uibModalInstance, task, PriorityService, TasksService) {
    $scope.task = task;
    $scope.priorities = PriorityService.priorityEnum;
    $scope.selected = {
        task: $scope.task[0]
    };

    $scope.ok = function (task) {
        TasksService.add(task);
        $uibModalInstance.close(task);
    };

    $scope.update = function (task) {
        $uibModalInstance.close(task);
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}

module.exports = ModalInstanceCtrl;
