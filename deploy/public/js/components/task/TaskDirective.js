function headerDirective() {
    return {
        controller: 'TaskCtrl',
        controllerAs: 'taskCtrl',
        templateUrl: 'js/components/task/task.html'
    }
}

module.exports = headerDirective;