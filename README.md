# ToDo Manager#

### Clone application
Clone repository using [git](http://git-scm.com/):

```
git clone https://oleksiy_stetsenko@bitbucket.org/oleksiy_stetsenko/todomanager.git
cd todomanager
```

### Install Dependencies

* We get the tools we depend upon via `npm`, the [node package manager](https://www.npmjs.org).
* We get the clien-side dependencies via `bower`, a [client-side code package manager](https://bower.io).
* For run server install [nodejs](https://nodejs.org/en/)

We have preconfigured `npm` to automatically run `bower` so we can simply do:

```
npm install
```

Behind the scenes this will also call `bower install`.  You should find that you have two new
folders in your project.

* `node_modules` - contains the npm packages for the tools we need
* `src/public/bower_components` - contains angular and boostrap framework's files

### Run the Application

We have preconfigured the project with a simple development web server.  The simplest way to start
this server is:

```
npm start
```

Now browse to the app at `http://localhost:3000`.

###  See demo

Deployed application available [here](https://whispering-falls-49560.herokuapp.com/)