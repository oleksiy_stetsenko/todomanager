var
    gulp = require('gulp'),
    gulpsync = require('gulp-sync')(gulp),
    del = require('del'),
    uglify = require('gulp-uglify'),
    print = require('gulp-print'),
    jade = require('gulp-jade'),
    wrap = require('gulp-wrap'),
    rename = require('gulp-rename'),
    path = require('path');

gulp.task('default', ['deploy']);

/// DEPLOY

gulp.task('deploy:clear', function () {
    return del(['deploy/*', '!deploy/.git/']);
});

gulp.task('deploy:js', function () {
    return gulp.src(['./src/public/**/*.js', '!./src/public/js/app.js'])
        .pipe(gulp.dest('./deploy/public'));
});

gulp.task('deploy:routes', function () {
    return gulp.src(['./src/routes/**/*'])
        .pipe(uglify())
        .pipe(gulp.dest('./deploy/routes'));
});

gulp.task('deploy:css', function () {
    return gulp.src(['./src/public/**/*.css', './src/public/**/*.less'])
        .pipe(gulp.dest('./deploy/public'));
});

gulp.task('deploy:bootstrap-fonts', function () {
    return gulp.src(['./src/public/bower_components/bootstrap/dist/fonts/**/*'])
        .pipe(gulp.dest('./deploy/public/bower_components/bootstrap/dist/fonts/'));
});

gulp.task('deploy:static', function () {
    return gulp.src(['./src/public/**/*.html'])
        .pipe(gulp.dest('./deploy/public/'));
});

gulp.task('deploy:views', function () {
    return gulp.src(['./src/views/**/*'])
        .pipe(gulp.dest('./deploy/views'));
});

gulp.task('deploy:common', function () {
    return gulp.src(['./src/*.js', './package*.json'])
        .pipe(gulp.dest('./deploy/'));
});

gulp.task('deploy:copy', [
    'deploy:js',
    'deploy:routes',
    'deploy:css',
    'deploy:bootstrap-fonts',
    'deploy:static',
    'deploy:views',
    'deploy:common'
]);

gulp.task('deploy', gulpsync.sync(['deploy:clear', ['deploy:copy']]));
