function TaskCtrl(TasksService, PriorityService, $uibModal, $scope) {

    $scope.taskActions = [
        {name: 'delete'},
        {name: 'edit'},
        {name: 'add sub task'}
    ];

    $scope.priorities = PriorityService.priorityEnum;
    $scope.model = TasksService;

    $scope.addTaskModal = function () {
        $uibModal.open({
            animation: true,
            templateUrl: 'js/components/task/parts/task-modal.html',
            controller: 'ModalInstanceCtrl',
            resolve: {
                task: function () {
                    return {
                        isNew: true,
                        // priority: $scope.priorities.properties[$scope.priorities.MEDIUM],
                        dueDateTime: new Date(),
                        timeTracking: {
                            spentTime: 0,
                            timeFormat: "yyyy/MM/dd HH:mm:ss"
                        }
                    };
                }
            }
        });
    };

    $scope.doneTask = function (task) {
        TasksService.remove(task)
    };

    $scope.editTask = function (task) {
        $uibModal.open({
            animation: true,
            templateUrl: 'js/components/task/parts/task-modal.html',
            controller: 'ModalInstanceCtrl',
            resolve: {
                task: function () {
                    task.isNew = false;
                    return task;
                }
            }
        });
    };

    $scope.setAction = function (action, task) {
        if (action.name === 'delete') {
            TasksService.remove(task);
        } else if (action.name === 'edit') {
            $scope.editTask(task);
        } else {
            console.log("todo: add extra actions");
        }
    };
}

module.exports = TaskCtrl;
