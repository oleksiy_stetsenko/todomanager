function ModalInstanceCtrl($scope, $uibModalInstance, task, PriorityService, TasksService) {
    $scope.task = angular.copy(task);
    $scope.priorities = PriorityService.priorityEnum;
    $scope.selected = {
        task: $scope.task[0]
    };

    $scope.ok = function (task) {
        TasksService.add(task);
        $uibModalInstance.close(task);
    };

    $scope.update = function (task) {
        TasksService.updateTask(task);
        $uibModalInstance.close(task);
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}

module.exports = ModalInstanceCtrl;