function ProjectsService() {

    var service = this;
    service.projects = [
        {
            id: 1,
            title: 'project1',
            tasks: [
                {
                    "title": "foobar",
                    "priority": "1",
                    "comments": "2",
                    "dueDateTime": "2016-05-18T06:02:00.000Z",
                    "tasks": [
                        {
                            id: 5,
                            title: 'sub-task-id-5',
                            tasks: []
                        },
                        {
                            id: 6,
                            title: 'sub-task-id-6',
                            tasks: []
                        }
                    ],
                    "timeTracking": {
                        "estimatedTime": 60,
                        "spentTime": 20,
                        "timeFormat": "yyyy/MM/dd HH:mm:ss"
                    }
                }, {
                    "title": "second",
                    "priority": "3",
                    "dueDateTime": "2016-05-11T23:01:00.000Z",
                    "comments": "comment2",
                    "tasks": []
                }
            ]
        },
        {
            id: 2,
            title: 'super-project2',
            tasks: [
                {
                    id: 3,
                    title: 'super-task3',
                    tasks: []
                },
                {
                    id: 4,
                    title: 'super-task4',
                    tasks: []
                }]
        }
    ];

    service.get = function () {
        return service.projects;
    };

    service.getStatistic = function (project) {
        var result = {
            count: 0,
            sumEstimated: 0,
            sumRemaining: 0
        };

        getInfo(project.tasks);
        return result;

        function getInfo(task) {
            task.forEach(function (subTask) {
                if (subTask.tasks) {
                    getInfo(subTask.tasks)
                }
                result.count++;

                if (isEstimatedTimeExist(subTask)) {
                    result.sumEstimated += subTask.timeTracking.estimatedTime;
                    if (isTimeSpentExist(subTask)) {
                        result.sumRemaining += subTask.timeTracking.estimatedTime - subTask.timeTracking.spentTime
                    } else {
                        result.sumRemaining += subTask.timeTracking.estimatedTime;
                    }
                }
            });
        }

        function isEstimatedTimeExist(subTask) {
            return subTask.timeTracking && subTask.timeTracking.estimatedTime;
        }

        function isTimeSpentExist(subTask) {
            return subTask.timeTracking.spentTime;
        }

    };

    service.getTasksByProject = function (project) {
        return service.projects.filter(function (item) {
            if (item === project) {
                return item.tasks;
            }
        })
    };

    service.addNewProject = function (project) {
        service.projects.push({
            title: project.title,
            tasks: []
        })
    };

    service.update = function (project) {
        service.projects.forEach(function (item, index) {
            if (item.id == project.id) {
                service.projects[index] = project;
            }
        });
    };

    service.remove = function (project) {
        service.projects = service.projects.filter(function (item) {
            return item.id !== project.id;
        });
    }
}

module.exports = ProjectsService;
