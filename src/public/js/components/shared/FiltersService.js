function FilterService() {

    var service = this;
    service.filters = [
        {
            id: 1,
            title: "Low priority",
            value: "low"
        },
        {
            id: 2,
            title: "Middle priority",
            value: "middle"
        },
        {
            id: 3,
            title: "High priority",
            value: "high"
        },
        {
            id: 4,
            title: "All tasks",
            value: "all"
        },
        {
            id: 5,
            title: "For today",
            value: "today"
        },
        {
            id: 6,
            title: "For week",
            value: "week"
        }
    ];
}

module.exports = FilterService;