function ProjectModalInstanceCtrl($scope, $uibModalInstance, project, ProjectsService) {

    $scope.project = angular.copy(project);
    $scope.statistic = ProjectsService.getStatistic(project);

    $scope.ok = function (project) {
        ProjectsService.update(project);
        $uibModalInstance.close(project);
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}

module.exports = ProjectModalInstanceCtrl;
