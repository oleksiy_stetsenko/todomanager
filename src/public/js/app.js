var angular = require('../bower_components/angular');

var ngSanitize = require('../bower_components/angular-sanitize/angular-sanitize');

var uiBootstap = require('../bower_components/angular-bootstrap/ui-bootstrap-tpls.min');
var uiTree = require('../bower_components/angular-ui-tree/dist/angular-ui-tree.min');

// Controllers
var TaskCtrl = require('./components/task/TaskCtrl');
var ProjectCtrl = require('./components/project/ProjectCtrl');
var ModalInstanceCtrl = require('./components/task/ModalInstanceCtrl');
var ProjectModalInstanceCtrl = require('./components/project/ProjectModalInstanceCtrl');

//Services
var ProjectsService = require('./components/shared/ProjectsService');
var TasksService = require('./components/shared/TasksService');
var PriorityService = require('./components/shared/PriorityService');
var FilterService = require('./components/shared/FiltersService');

//Directives
var taskDirective = require('./components/task/TaskDirective');
var projectDirective = require('./components/project/ProjectDirective');

angular
    .module('todoManager', ['ngSanitize', 'ui.bootstrap', 'ui.tree'])

    .directive('tasks', taskDirective)
    .directive('projects', projectDirective)

    .service('ProjectsService', ProjectsService)
    .service('TasksService', TasksService)
    .service('PriorityService', PriorityService)
    .service('FilterService', FilterService)

    .controller('TaskCtrl', TaskCtrl)
    .controller('ProjectsCtrl', ProjectCtrl)
    .controller('ModalInstanceCtrl', ModalInstanceCtrl)
    .controller('ProjectModalInstanceCtrl', ProjectModalInstanceCtrl);
